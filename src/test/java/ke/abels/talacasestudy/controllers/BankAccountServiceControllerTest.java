/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.abels.talacasestudy.controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import ke.abels.talacasestudy.idata.IAccountRepository;
import ke.abels.talacasestudy.idata.IAccountTransactionRepository;
import ke.abels.talacasestudy.models.Account;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import static org.junit.Assert.*;

/**
 *
 * @author salama
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class BankAccountServiceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private IAccountRepository accountRepository;
    @Autowired
    private IAccountTransactionRepository accountTransactionRepository;

    public BankAccountServiceControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
        BankAccountServiceControllerTest.tuncateAccountTransactions();
    }

    /**
     * Test of balance method, of class BankAccountServiceController.
     */
    @Test
    public void testBalance() throws Exception {
        System.err.println("> Testing method balance()");

        System.err.println(">>> Testing method balance() empty account string");
        String expected = "Error: Invalid account specified.";
        String result = this.restTemplate.getForEntity("/bank_account/balance", String.class).getBody();
        assertNotNull(result);
        assertNotEquals(expected, result);
        assertEquals("Error: No account specified.", result);

        System.err.println(">>> Testing method balance() wrong account string");
        expected = "Error: Invalid account specified.";
        result = this.restTemplate.getForEntity("/bank_account/balance?accountNumber=", String.class).getBody();
        assertNotNull(result);
        assertEquals(expected, result);

        System.err.println(">>> Testing method balance() correct account string");
        expected = "Success: Your account balance is Kes. " + accountRepository.findByAccountNumber("254727452403").getBalance();
        result = this.restTemplate.getForEntity("/bank_account/balance?accountNumber=254727452403", String.class).getBody();
        assertNotNull(result);
        assertEquals(expected, result);
    }

    /**
     * Test of deposit method, of class BankAccountServiceController.
     */
    @Test
    public void testDeposit() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 1);

        System.out.println("deposit");
        System.err.println("> Testing method deposit()");

        System.err.println(">>> Testing method deposit() max deposit amount per transaction");
        Account account = accountRepository.findByAccountNumber("254727452403");
        double amount = 40001.00;
        String expected = "Error-MaxDepositPerTransaction: Deposit amount of Kes. " + amount + " has exceeded the per transaction limit allowed of " + account.getMaxDepositPerTransaction() + " for your account";
        String result = this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        assertEquals(expected, result);

        System.err.println(">>> Testing method deposit() depsoit money into the account");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 40000.00;
        result = this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        account = accountRepository.findByAccountNumber("254727452403");
        expected = "Success: You have successfully desposited Kes. " + amount + " to your account. Your account balance is Kes. " + account.getBalance();
        assertEquals(expected, result);

        System.err.println(">>> Testing method deposit() max deposits per day");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 30000.00;
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        result = this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        expected = "Error-MaxDepositsPerDay: You have reached your limit for daily deposit transactions allowed for your account.";
        assertEquals(expected, result);

        System.err.println(">>> Testing method deposit() max amount deposited per day");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 40000.00;
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        result = this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        account = accountRepository.findByAccountNumber("254727452403");
        Double deposit = accountTransactionRepository.sumByByAccountAndCreatedAtGreaterThanAndTransactionType(account, (int) (calendar.getTimeInMillis() / 1000), "DEPOSIT");
        expected = "Error-MaxDailyDeposit: Deposit amount of Kes. " + amount + " takes your total deposit for the day to Kes. " + (deposit + amount) + " which exceeds the limit for the total amount allowed to be deposited for the day of Kes. " + account.getMaxDailyDeposit() + " for your account";
        assertEquals(expected, result);

        System.err.println(">>> Testing method deposit() deposit amount > 0.00");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 0.00;
        result = this.restTemplate.getForEntity("/bank_account/deposit?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        expected = "Error: The minimum amount allowed to be deposited is Kes 1.0.";
        assertEquals(expected, result);
    }

    /**
     * Test of withdraw method, of class BankAccountServiceController.
     */
    @Test
    public void testWithdraw() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 1);

        System.out.println("whithdraw");
        System.err.println("> Testing method withdraw()");

        System.err.println(">>> Testing method withdraw() withdraw amount greater than available balance");
        Account account = accountRepository.findByAccountNumber("254727452403");
        account.setBalance(19000.00);
        accountRepository.save(account);
        double amount = account.getBalance() + 1.00;
        String expected = "Error-InsufficientBalance: Your balance is Kes. " + account.getBalance();
        String result = this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        assertEquals(expected, result);

        System.err.println(">>> Testing method withdraw() max withdrawal amount per transaction");
        account = accountRepository.findByAccountNumber("254727452403");
        account.setBalance(200000.00);
        accountRepository.save(account);
        amount = 20001.00;
        expected = "Error-MaxWithdrawalPerTransaction: Withdrawal amount of Kes. " + amount + " has exceeded the per transaction limit allowed of " + account.getMaxWithdrawalPerTransaction() + " for your account.";
        result = this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        assertEquals(expected, result);

        System.err.println(">>> Testing method withdraw() withdraw money from the account");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 20000.00;
        result = this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        account = accountRepository.findByAccountNumber("254727452403");
        expected = "Success: You have successfully withdrawn Kes. " + amount + " for your account. Your account balance is Kes. " + account.getBalance();
        assertEquals(expected, result);

        System.err.println(">>> Testing method withdraw() max withdrawals per day");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 15000.00;
        this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        result = this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        expected = "Error-MaxWithdrawalsPerDay: You have reached your limit for daily withdrawal transactions allowed for your account.";
        assertEquals(expected, result);

        System.err.println(">>> Testing method withdraw() max amount withdrawn per day");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 20000.00;
        this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        result = this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        account = accountRepository.findByAccountNumber("254727452403");
        Double withdrawal = accountTransactionRepository.sumByByAccountAndCreatedAtGreaterThanAndTransactionType(account, (int) (calendar.getTimeInMillis() / 1000), "WITHDRAWAL");
        expected = "Error-MaxDailyWithdrawal: Withdrawal amount of Kes. " + amount + " takes your total withdrawal for the day to Kes. " + (withdrawal + amount) + " which exceeds the limit for the total amount allowed to be withdrawn for the day of Kes. " + account.getMaxDailyWithdrawal() + " for your account.";
        assertEquals(expected, result);

        System.err.println(">>> Testing method withdraw() withdraw amount > 0.00");
        BankAccountServiceControllerTest.tuncateAccountTransactions();
        amount = 0.00;
        result = this.restTemplate.getForEntity("/bank_account/withdraw?accountNumber=254727452403&amount=" + amount, String.class).getBody();
        expected = "Error: The minimum amount allowed to be withdrawn is Kes 1.0.";
        assertEquals(expected, result);
    }

    /**
     *
     */
    private static void tuncateAccountTransactions() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tala_case_study", "root", "amkatwende");

            preparedStatement = connection.prepareStatement("TRUNCATE account_transaction");
            preparedStatement.execute();
        } catch (ClassNotFoundException classNotFoundException) {
            System.out.println(classNotFoundException.getLocalizedMessage());
            classNotFoundException.printStackTrace();
        } catch (SQLException sQLException) {
            System.out.println(sQLException.getLocalizedMessage());
            sQLException.printStackTrace();
        } catch (Exception exception) {
            System.out.println(exception.getLocalizedMessage());
            exception.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException sQLException) {
                    System.out.println(sQLException.getLocalizedMessage());
                    sQLException.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sQLException) {
                    System.out.println(sQLException.getLocalizedMessage());
                    sQLException.printStackTrace();
                }
            }
            connection = null;
        }
    }
}
