/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.abels.talacasestudy.idata;

import java.util.List;
import ke.abels.talacasestudy.models.Account;
import ke.abels.talacasestudy.models.AccountTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author salama
 */
public interface IAccountTransactionRepository extends CrudRepository<AccountTransaction, Long> {

    /**
     *
     * @param account
     * @param createdAt
     * @param transactionType
     * @return
     */
    List<AccountTransaction> findByAccountAndCreatedAtGreaterThanAndTransactionType(Account account, int createdAt, String transactionType);

    /**
     *
     * @param account
     * @param createdAt
     * @param transactionType
     * @return
     */
    Integer countByAccountAndCreatedAtGreaterThanAndTransactionType(Account account, int createdAt, String transactionType);

    /**
     *
     * @param account
     * @param createdAt
     * @param transactionType
     * @return
     */
    @Query("SELECT SUM(ac.amount) FROM AccountTransaction ac WHERE ac.account=:account AND ac.createdAt>:createdAt AND ac.transactionType=:transactionType")
    Double sumByByAccountAndCreatedAtGreaterThanAndTransactionType(@Param("account") Account account, @Param("createdAt") int createdAt, @Param("transactionType") String transactionType);

}
