/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.abels.talacasestudy.idata;

import ke.abels.talacasestudy.models.Account;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author salama
 */
public interface IAccountRepository extends CrudRepository<Account, Long> {

    /**
     *
     * @param accountNumber
     * @return
     */
    Account findByAccountNumber(String accountNumber);
}
