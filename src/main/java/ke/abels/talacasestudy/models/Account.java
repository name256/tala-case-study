package ke.abels.talacasestudy.models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author salama
 */
@Entity
@Table(indexes = {
    @Index(columnList = "accountNumber")
})
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private String accountNumber;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    private double balance;
    private double maxDailyDeposit;
    private double maxDepositPerTransaction;
    private int maxDepositsPerDay;
    private double maxDailyWithdrawal;
    private double maxWithdrawalPerTransaction;
    private int maxWithdrawalsPerDay;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<AccountTransaction> accountTransactions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getMaxDailyDeposit() {
        return maxDailyDeposit;
    }

    public void setMaxDailyDeposit(double maxDailyDeposit) {
        this.maxDailyDeposit = maxDailyDeposit;
    }

    public double getMaxDepositPerTransaction() {
        return maxDepositPerTransaction;
    }

    public void setMaxDepositPerTransaction(double maxDepositPerTransaction) {
        this.maxDepositPerTransaction = maxDepositPerTransaction;
    }

    public int getMaxDepositsPerDay() {
        return maxDepositsPerDay;
    }

    public void setMaxDepositsPerDay(int maxDepositsPerDay) {
        this.maxDepositsPerDay = maxDepositsPerDay;
    }

    public double getMaxDailyWithdrawal() {
        return maxDailyWithdrawal;
    }

    public void setMaxDailyWithdrawal(double maxDailyWithdrawal) {
        this.maxDailyWithdrawal = maxDailyWithdrawal;
    }

    public double getMaxWithdrawalPerTransaction() {
        return maxWithdrawalPerTransaction;
    }

    public void setMaxWithdrawalPerTransaction(double maxWithdrawalPerTransaction) {
        this.maxWithdrawalPerTransaction = maxWithdrawalPerTransaction;
    }

    public int getMaxWithdrawalsPerDay() {
        return maxWithdrawalsPerDay;
    }

    public void setMaxWithdrawalsPerDay(int maxWithdrawalsPerDay) {
        this.maxWithdrawalsPerDay = maxWithdrawalsPerDay;
    }

    public List<AccountTransaction> getAccountTransactions() {
        return accountTransactions;
    }

    public void setAccountTransactions(List<AccountTransaction> accountTransactions) {
        this.accountTransactions = accountTransactions;
    }

}
