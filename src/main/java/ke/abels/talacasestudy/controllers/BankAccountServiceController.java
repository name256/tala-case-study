/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.abels.talacasestudy.controllers;

import java.util.Calendar;
import ke.abels.talacasestudy.idata.IAccountRepository;
import ke.abels.talacasestudy.idata.IAccountTransactionRepository;
import ke.abels.talacasestudy.models.Account;
import ke.abels.talacasestudy.models.AccountTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author salama
 */
@RestController
public class BankAccountServiceController {

    @Autowired
    IAccountRepository accountRepository;
    @Autowired
    IAccountTransactionRepository accountTransactionRepository;

    /**
     *
     * @param accountNumber
     * @return
     */
    @RequestMapping("/bank_account/balance")
    @ResponseBody
    public String balance(String accountNumber) {
        if (accountNumber == null) {
            // return error in the event the specified account doesn't exist
            return "Error: No account specified.";
        }
        Account account = null;
        try {
            // initialize the account and query whether it exists
            account = accountRepository.findByAccountNumber(accountNumber);
        } catch (Exception exception) {
            return exception.getLocalizedMessage();
        }
        if (account == null) {
            // return error in the event the specified account doesn't exist
            return "Error: Invalid account specified.";
        }
        // return a successful response with the account balance if the eccount is found
        return "Success: Your account balance is Kes. " + account.getBalance();
    }

    /**
     *
     * @param accountNumber
     * @param amount
     * @return
     */
    @RequestMapping("/bank_account/deposit")
    @ResponseBody
    public String deposit(String accountNumber, Double amount) {
        // set time to just before modnight the previous day
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 1);

        if (accountNumber == null) {
            // return error in the event the specified account doesn't exist
            return "Error: No account specified.";
        }
        
        Account account = null;
        try {
            // initialize the account and query whether it exists
            account = accountRepository.findByAccountNumber(accountNumber);
        } catch (NullPointerException nullPointerException) {
            //TODO log that no account information was returned
        } catch (Exception exception) {
            return exception.getLocalizedMessage();
        }
        if (account == null) {
            // return error in the event the specified account doesn't exist
            return "Error-InvalidAccountSpecified.";
        }
        // check whether the maximum amount per deposit has been reached
        if (amount > account.getMaxDepositPerTransaction()) {
            return "Error-MaxDepositPerTransaction: Deposit amount of Kes. " + amount + " has exceeded the per transaction limit allowed of " + account.getMaxDepositPerTransaction() + " for your account";
        }
        // check whether the maximum number of deposits for the day has been reached
        Integer deposits = 0;
        try {
            deposits = accountTransactionRepository.countByAccountAndCreatedAtGreaterThanAndTransactionType(account, (int) (calendar.getTimeInMillis() / 1000), "DEPOSIT");
        } catch (Exception exception) {
            return "Exception-MaxDepositsPerDay: " + exception.getLocalizedMessage();
        }
        if (deposits == account.getMaxDepositsPerDay()) {
            return "Error-MaxDepositsPerDay: You have reached your limit for daily deposit transactions allowed for your account.";
        }
        // check whether the maximum deposit for the day has been reached
        double deposit = 0.0;
        try {
            deposit = accountTransactionRepository.sumByByAccountAndCreatedAtGreaterThanAndTransactionType(account, (int) (calendar.getTimeInMillis() / 1000), "DEPOSIT");
        } catch (NullPointerException nullPointerException) {
            //TODO log and show that there have been no deposits to the account in the transactions table
            deposit = 0.0;
        } catch (Exception exception) {
            return "Exception-MaxDailyDeposits: " + exception.getLocalizedMessage();
        }
        if (deposit == account.getMaxDailyDeposit()) {
            return "Error-MaxDailyDeposits: You have reached your limit for the daily amount allowed to be deposited for your account.";
        }
        // check whether the maximum deposit for the day has been reached once the money is deposited
        double totalDeposit = deposit + amount;
        if (totalDeposit > account.getMaxDailyDeposit()) {
            return "Error-MaxDailyDeposit: Deposit amount of Kes. " + amount + " takes your total deposit for the day to Kes. " + totalDeposit + " which exceeds the limit for the total amount allowed to be deposited for the day of Kes. " + account.getMaxDailyDeposit() + " for your account";
        }
        if (amount > 0.0) {
            // deposit money into the account
            AccountTransaction accountTransaction;
            try {
                accountTransaction = new AccountTransaction();
                accountTransaction.setAccount(account);
                accountTransaction.setAmount(amount);
                accountTransaction.setBalance(amount + account.getBalance());
                accountTransaction.setTransactionType("DEPOSIT");
                accountTransaction.setCreatedAt((int) (Calendar.getInstance().getTimeInMillis() / 1000));
                accountTransactionRepository.save(accountTransaction);

                account.setBalance(amount + account.getBalance());
                accountRepository.save(account);
            } catch (Exception exception) {
                return "Exception-AccountDeposit: " + exception.getLocalizedMessage();
            }
        } else {
            // return a error message if the amount to be deposited is 0
            return "Error: The minimum amount allowed to be deposited is Kes 1.0.";
        }
        // return a successful response with the account balance if the eccount is found
        return "Success: You have successfully desposited Kes. " + amount + " to your account. Your account balance is Kes. " + account.getBalance();
    }

    /**
     *
     * @param accountNumber
     * @param amount
     * @return
     */
    @RequestMapping("/bank_account/withdraw")
    @ResponseBody
    public String withdraw(String accountNumber, Double amount) {
        // set time to just before modnight the previous day
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 1);

        if (accountNumber == null) {
            // return error in the event the specified account doesn't exist
            return "Error: No account specified.";
        }
        
        Account account;
        try {
            // initialize the account and query whether it exists
            account = accountRepository.findByAccountNumber(accountNumber);
        } catch (Exception exception) {
            return exception.getLocalizedMessage();
        }
        if (account == null) {
            // return error in the event the specified account doesn't exist
            return "Error-InvalidAccountSpecified.";
        }
        // check whether the maximum amount per withdrawal has been reached
        if (amount > account.getMaxWithdrawalPerTransaction()) {
            return "Error-MaxWithdrawalPerTransaction: Withdrawal amount of Kes. " + amount + " has exceeded the per transaction limit allowed of " + account.getMaxWithdrawalPerTransaction() + " for your account.";
        }
        // check whether the maximum number of withdrawals for the day has been reached
        Integer deposits = 0;
        try {
            deposits = accountTransactionRepository.countByAccountAndCreatedAtGreaterThanAndTransactionType(account, (int) (calendar.getTimeInMillis() / 1000), "WITHDRAWAL");
        } catch (Exception exception) {
            return "Exception-MaxWithdrawalsPerDay: " + exception.getLocalizedMessage();
        }
        if (deposits == account.getMaxWithdrawalsPerDay()) {
            return "Error-MaxWithdrawalsPerDay: You have reached your limit for daily withdrawal transactions allowed for your account.";
        }
        // check whether the maximum deposit for the day has been reached
        double withdrawal = 0.0;
        try {
            withdrawal = accountTransactionRepository.sumByByAccountAndCreatedAtGreaterThanAndTransactionType(account, (int) (calendar.getTimeInMillis() / 1000), "WITHDRAWAL");
        } catch (NullPointerException nullPointerException) {
            //TODO log and show that there have been no deposits to the account in the transactions table
            withdrawal = 0.0;
        } catch (Exception exception) {
            return "Exception-MaxDailyWithdrawals: " + exception.getLocalizedMessage();
        }
        if (withdrawal == account.getMaxDailyWithdrawal()) {
            return "Error-MaxDailyWithdrawals: You have reached your limit for the daily amount allowed to be withdrawn for your account.";
        }
        // check whether the maximum deposit for the day has been reached once the money is deposited
        double totalWithdrawal = withdrawal + amount;
        if (totalWithdrawal > account.getMaxDailyWithdrawal()) {
            return "Error-MaxDailyWithdrawal: Withdrawal amount of Kes. " + amount + " takes your total withdrawal for the day to Kes. " + totalWithdrawal + " which exceeds the limit for the total amount allowed to be withdrawn for the day of Kes. " + account.getMaxDailyWithdrawal() + " for your account.";
        }
        // check whether the amount to be withdrawn is going to reduce the account balance to less than 0
        if ((account.getBalance() - amount) < 0) {
            return "Error-InsufficientBalance: Your balance is Kes. " + account.getBalance();
        }
        if (amount > 0.0) {
            // deposit money into the account
            AccountTransaction accountTransaction;
            try {
                accountTransaction = new AccountTransaction();
                accountTransaction.setAccount(account);
                accountTransaction.setAmount(amount);
                accountTransaction.setBalance(account.getBalance() - amount);
                accountTransaction.setTransactionType("WITHDRAWAL");
                accountTransaction.setCreatedAt((int) (Calendar.getInstance().getTimeInMillis() / 1000));
                accountTransactionRepository.save(accountTransaction);

                account.setBalance(account.getBalance() - amount);
                accountRepository.save(account);
            } catch (Exception exception) {
                return "Exception-AccountWithdrawal: " + exception.getLocalizedMessage();
            }
        } else {
            // return a error message if the amount to be deposited is 0
            return "Error: The minimum amount allowed to be withdrawn is Kes 1.0.";
        }
        // return a successful response with the account balance if the eccount is found
        return "Success: You have successfully withdrawn Kes. " + amount + " for your account. Your account balance is Kes. " + account.getBalance();
    }
}
