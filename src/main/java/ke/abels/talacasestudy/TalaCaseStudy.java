package ke.abels.talacasestudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TalaCaseStudy {

	public static void main(String[] args) {
		SpringApplication.run(TalaCaseStudy.class, args);
	}
}
