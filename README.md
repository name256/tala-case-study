# README #

This README documents the tala-case-study application that has been built using the SpringBoot platform to build a Bank Account rest web service.

### What is this repository for? ###

* The bank account web service is used to check for balance, deposit and withdraw money from a particular bank account.
* 1.0
* The project has been built using Java 1.8 and Maven3

### How do I get set up? ###

* To get a copy of the project run the command "git clone https://name256@bitbucket.org/name256/tala-case-study.git". Ensure that Git is installed on your computer.
* There is an application.properties file in the /src/resources folder of the project. This file holds the properties of the persistence configuration to the project database which is a mysql based. A tala_case_study.sql file can be found in the root folder of the application for use to run the project.
* Since this is a maven project, one can run tests using the command "mvn3 test".
* In order to deploy the application, first run the command "mvn3 clean install". Once the build operation is complete, there will be a target folder in the root folder of the project. Within that folder is a tala-case-study.jar file. To run the application execute the command "java -jar tala-case-study.jar". Once the application is deployed you can access and test the different endpoints to test the service at:
 1) Balance - http://localhost:8080/bank_account/balance - parameters > accountNumber (String);
 2) Deposit - http://localhost:8080/bank_account/deposit - parameters > accountNumber (String), amount (Double);
 3) Balance - http://localhost:8080/bank_account/withdraw - parameters > accountNumber (String), amount (Double);