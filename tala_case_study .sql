-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_number` varchar(255) NOT NULL,
  `balance` double NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `max_daily_deposit` double NOT NULL,
  `max_daily_withdrawal` double NOT NULL,
  `max_deposit_per_transaction` double NOT NULL,
  `max_deposits_per_day` int(11) NOT NULL,
  `max_withdrawal_per_transaction` double NOT NULL,
  `max_withdrawals_per_day` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDXflt7u0k5t3citp7q2or59b4ii` (`account_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `account` (`id`, `account_number`, `balance`, `first_name`, `last_name`, `max_daily_deposit`, `max_daily_withdrawal`, `max_deposit_per_transaction`, `max_deposits_per_day`, `max_withdrawal_per_transaction`, `max_withdrawals_per_day`) VALUES
(1,	'254727452403',	375000,	'Abel',	'Salama',	150000,	50000,	40000,	4,	20000,	3);

DROP TABLE IF EXISTS `account_transaction`;
CREATE TABLE `account_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `created_at` int(11) NOT NULL,
  `transaction_type` varchar(255) NOT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `createdAt` (`created_at`),
  KEY `transactionType` (`transaction_type`),
  KEY `FKqonh25s0w6r5cf8jq88m6kd8o` (`account_id`),
  CONSTRAINT `FKqonh25s0w6r5cf8jq88m6kd8o` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2017-01-05 19:40:44
